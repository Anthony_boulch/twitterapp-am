import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TwitterTimelineComponent } from './components/twitter-timeline/twitter-timeline.component';
import { TwitterMentionsComponent } from './components/twitter-mentions/twitter-mentions.component';
import { TweetComponent } from './components/tweet/tweet.component';
import { HomeComponent } from './components/home/home.component';
import { TendancesComponent } from './components/tendances/tendances.component'
import { SearchTendanceComponent } from './components/search-tendance/search-tendance.component';
import { AbonnementComponent } from './components/abonnement/abonnement.component';
import { FollowersComponent } from './components/followers/followers.component';

const routes: Routes = [
  {
    path: 'twitter_timeline',
    component: TwitterTimelineComponent
  },
  {
    path: 'twitter_mentions',
    component: TwitterMentionsComponent
  },
  {
    path: 'tendances',
    component: TendancesComponent
  },
  {
    path: 'searchtendance',
    component: SearchTendanceComponent
  },
  {
    path: 'abonnements',
    component: AbonnementComponent
  },
  {
    path: 'followers',
    component: FollowersComponent
  },
  { 
    path:"",
    component:HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
