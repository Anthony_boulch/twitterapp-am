import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Tweet } from './interfaces/tweet';

@Injectable({
  providedIn: 'root'
})
export class TwitterService {
  api_url = 'http://localhost:3000';
  tweet_url = '';
  tweet_hashtag = '';

  constructor(private http: HttpClient) { }

  /**
   * return infos users
   */
  getUserInfo(){
    return this.http.get<any>(this.api_url + "/userInfos").toPromise()
  }

  /**
   * return les derniers tweets (fil actu)
   */
  getTimeline(){
    return this.http
      .get<any>(this.api_url+'/home_timeline').toPromise()
  }
 
  /**
   * return les tweets dans lesquels j'ai été mentionné
   */
  getMentions() {
    return this.http
      .get<any>(this.api_url+'/mentions_timeline').toPromise()
 
  }

  /**
   * return les dernières tendances
   */
  getTendances() {
    return this.http
      .get<any>(this.api_url+'/tendances').toPromise()
 
  }

  /**
   * Permet de poster un tweet
   * @param tweetdata 
   */
  tweet(tweetdata: string): Observable<any>{
    return this.http.post<any>(`${this.api_url}/post_tweet/`, {status: tweetdata})
      .pipe(map(tweet => {
          return tweet;
      }));
  }

  /**
   * return les tweets rechercher avec la tendance
   * @param query 
   */
  getTweetTendance(query: string) {
    return this.http.get<any>(this.api_url + '/searchTendance?query=' + query).toPromise()
  }

  /**
   * return mes abonnements (follow)
   */
  getAbonnement(){
    return this.http.get<any>(this.api_url + '/abonnement').toPromise()
  }

  /**
   * return mes abonnés (followers)
   */
  getFollowers(){
    return this.http.get<any>(this.api_url + '/followers').toPromise()
  }
}
