import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TwitterService } from 'src/app/twitter.service';

@Component({
  selector: 'app-search-tendance',
  templateUrl: './search-tendance.component.html',
  styleUrls: ['./search-tendance.component.css']
})
export class SearchTendanceComponent implements OnInit {
  tweetTendance: any;
  tweetHashtag: any;
  url: string;

  constructor(private api: TwitterService) { }

  ngOnInit() {
    this.tweetHashtag = this.api.tweet_hashtag;
    this.getTweetsTendance(this.api.tweet_url);
  }

  /**
   * Permet de rechercher les derniers tweets de la tendance grâce au service
   * @param query 
   */
  getTweetsTendance(query:string) {
    this.api.getTweetTendance(query).then(response => {
      this.tweetTendance = response.data.statuses;
    });
  }


}
