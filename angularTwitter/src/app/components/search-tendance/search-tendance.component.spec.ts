import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTendanceComponent } from './search-tendance.component';

describe('SearchTendanceComponent', () => {
  let component: SearchTendanceComponent;
  let fixture: ComponentFixture<SearchTendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchTendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
