import { Component, OnInit } from '@angular/core';
import { TwitterService } from 'src/app/twitter.service';

@Component({
  selector: 'app-abonnement',
  templateUrl: './abonnement.component.html',
  styleUrls: ['./abonnement.component.css']
})
export class AbonnementComponent implements OnInit {
  abonnements: any;
  constructor(private api: TwitterService) { }

  ngOnInit() {
    this.getAbonnement();
  }


  /**
   * Fonction permettant de récupérer les personnes que j'ai follow grâce au service
   */
  getAbonnement() {
    this.api.getAbonnement().then(response => {
      this.abonnements = response.data.users;
      console.log(this.abonnements);
    });
  }

}
