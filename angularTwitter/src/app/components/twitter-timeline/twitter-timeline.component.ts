import { Injectable } from '@angular/core';
import { Component, OnInit, Input } from '@angular/core';
import { TwitterService } from '../../twitter.service';
import { Tweet } from '../../interfaces/tweet';


@Component({
  selector: 'app-twitter-timeline',
  templateUrl: './twitter-timeline.component.html',
  styleUrls: ['./twitter-timeline.component.css']
})
@Injectable()
export class TwitterTimelineComponent implements OnInit {
  myTimeline: any;


  constructor(private api: TwitterService) { }
  ngOnInit() {
    this.getTwitterTimeline();
  }

  /**
   * Fonction permettant de récupérer les derniers tweets en utilisant le service
   */
  getTwitterTimeline(): void {
    this.api.getTimeline()
    .then((response) => {
      console.log(response.data);
      this.myTimeline = response.data
    })
  }
}
