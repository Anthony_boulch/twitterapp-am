import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TwitterService } from 'src/app/twitter.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tendances',
  templateUrl: './tendances.component.html',
  styleUrls: ['./tendances.component.css']
})
export class TendancesComponent implements OnInit {

  tendances: any;
  url: string;
  @Output() tendanceUrl = new EventEmitter<string>();


  constructor(private api: TwitterService, public router: Router) { }
  ngOnInit() {
    this.getTwitterTendances();
  }

  /**
   * Fonction permettant de récupérer des informations de la tendance
   * @param id 
   */
  getTendance(id: number){
    this.api.tweet_url = this.tendances[id].query;
    this.api.tweet_hashtag = this.tendances[id].name;
  }

  /**
   * Permet de récupérer les dernières tendances avec le service
   */
  getTwitterTendances(): void {
    this.api.getTendances()
    .then((response) => {
      this.tendances = response.data[0].trends
    })
  }

}
