import { Component, OnInit } from '@angular/core';
import { TwitterService } from 'src/app/twitter.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  userinfo_name: any;
  userinfo_picture: any;
  constructor(private api: TwitterService) { }

  ngOnInit() {
    this.getUserInfos();
  }

  /**
   * Permet de récupérer les infos utilisateur à mettre dans la navbar (img + name)
   */
  getUserInfos() {
    this.api.getUserInfo().then(response => {
      this.userinfo_name = response.data.name;
      this.userinfo_picture = response.data.profile_image_url;
      console.log(this.userinfo_picture);
    });
  }

}
