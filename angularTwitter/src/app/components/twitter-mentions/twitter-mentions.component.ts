import { Component, OnInit } from '@angular/core';
import { TwitterService } from 'src/app/twitter.service';

@Component({
  selector: 'app-twitter-mentions',
  templateUrl: './twitter-mentions.component.html',
  styleUrls: ['./twitter-mentions.component.css']
})
export class TwitterMentionsComponent implements OnInit {

  myMentions: any;


  constructor(private api: TwitterService) { }
  ngOnInit() {
    this.getTwitterMentions();
  }

  /**
   * Fonction permettant de récupérer les derniers tweets dans lesquels j'ai été mentionné en utilisant le service
   */
  getTwitterMentions(): void {
    this.api.getMentions()
    .then((response) => {
      console.log(response.data);
      this.myMentions = response.data
    })
  }

}
