import { Component, OnInit } from '@angular/core';
import { TwitterService } from 'src/app/twitter.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  tweetForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  myTimeline: any;

  constructor(private api: TwitterService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getTwitterTimeline();
    this.tweetForm = this.formBuilder.group({
      tweetdata: ['', Validators.required]
    });
  }

  getTwitterTimeline(): void {
    this.api.getTimeline()
    .then((response) => {
      console.log(response.data);
      this.myTimeline = response.data
    })
  }

  /**
   * Fonction permettant de get la valeur entrer dans le formulaire
   */
  get f() { return this.tweetForm.controls; }
 

  /**
   * Fonction qui post le tweet après le submit du formulaire
   */
  onSubmit() {
    this.submitted = true;

    //Stop si le formulaire est invalide
    if (this.tweetForm.invalid) {
        return;
    }

    this.loading = true;
    this.api.tweet(this.f.tweetdata.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log("yes");
          window.location.reload();
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  } 

}
