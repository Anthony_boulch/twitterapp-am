import { Component, OnInit } from '@angular/core';
import { TwitterService } from 'src/app/twitter.service';

@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.css']
})
export class FollowersComponent implements OnInit {
  followers: any;
  constructor(private api: TwitterService) { }

  ngOnInit() {
    this.getFollowers();
  }

  /**
   * Fonction permettant de récupérer les followers grâce au service
   */
  getFollowers() {
    this.api.getFollowers().then(response => {
      this.followers = response.data.users;
      console.log(this.followers);
    });
  }

}
