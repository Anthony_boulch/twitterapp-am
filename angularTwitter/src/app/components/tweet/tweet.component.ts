import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TwitterService } from 'src/app/twitter.service';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.css']
})
export class TweetComponent implements OnInit {
  tweetForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(private api: TwitterService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.tweetForm = this.formBuilder.group({
      tweetdata: ['', Validators.required]
    });
  }


  /**
   * Fonction permettant de get la valeur entrer dans le formulaire
   */
  get f() { return this.tweetForm.controls; }
 

  /**
   * Fonction qui post le tweet après le submit du formulaire
   */
  onSubmit() {
    this.submitted = true;

    //Stop si le formulaire est invalide
    if (this.tweetForm.invalid) {
        return;
    }

    this.loading = true;
    this.api.tweet(this.f.tweetdata.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log("yes")
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  } 

}
