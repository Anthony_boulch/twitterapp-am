import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TwitterTimelineComponent } from './components/twitter-timeline/twitter-timeline.component';
import { TwitterMentionsComponent } from './components/twitter-mentions/twitter-mentions.component';
import { TweetComponent } from './components/tweet/tweet.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TendancesComponent } from './components/tendances/tendances.component';
import { SearchTendanceComponent } from './components/search-tendance/search-tendance.component';
import { AbonnementComponent } from './components/abonnement/abonnement.component';
import { FollowersComponent } from './components/followers/followers.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    TwitterTimelineComponent,
    TwitterMentionsComponent,
    TweetComponent,
    TendancesComponent,
    SearchTendanceComponent,
    AbonnementComponent,
    FollowersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
