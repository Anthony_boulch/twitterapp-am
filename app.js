const express = require('express');
const Twitter = require('twit');
const app = express();

/* écoute sur le port 3000 */
app.listen(3000, () => console.log('Server running on port 3000'));


/* Le client twitter avec clé API, clé secrète, le token, et le token secret (compte anthony boulc'h) */
const client = new Twitter({
    consumer_key: 'bFD7r4voIWQuRl3K7zrJy4M3c',
    consumer_secret: 'EYTryrwrJM3KDs6onNoS6MievrufDbu1BLYYD9spkCO7daYlLb',
    access_token: '864803031996674048-1x2lWzfmWj4c8dP3rNvpg31IPTndXuG',
    access_token_secret: '7ePOPRu0zKwZAfjPBjdNsodeFWyIeYDQmgd9SUD5cYc6a'
  });

/* Compte Alexandre Mouriec (emprunt car mon compte était vide) */
  // const client = new Twitter({
  //   consumer_key: 'Kjf5igh06GTHhNQFaz3A4OKtx',
  //   consumer_secret: 'DHRGOY6Ch60nO3W117kAozEbnVsFtVOB0AVTR2gQCDzsMXxB08',
  //   access_token: '2871118510-MS2dVZQLYuMBfYBWCG8ivOhSjyrd1Sd3UtSrqSb',
  //   access_token_secret: 'g3iK5Usywub4gDa9H7PRRfIRJOqG6KcXtnOzUN4EgjKt3'
  // });


app.use(require('cors')());
app.use(require('body-parser').json());


/**
 * GET les 50 derniers tweets (fil d'actualité) 
 */
app.get('/home_timeline', (req, res) => {
  const params = { tweet_mode: 'extended', count: 50 };
  
  client
    .get(`statuses/home_timeline`, params)
    .then(timeline => {
        
      res.send(timeline);
    })
    .catch(error => {
    res.send(error);
  });
    
});


/**
 * GET les 50 derniers tweets dans lesquels j'ai été mentionné 
 */
app.get('/mentions_timeline', (req, res) => {
  const params = { tweet_mode: 'extended', count: 50 };
  
  client
    .get(`statuses/mentions_timeline`, params)
    .then(timeline => {
      
      res.send(timeline);
    })
    .catch(error => {
    res.send(error);
  });
    
});


/* GET les tweets par hastag (50 tweets affichés)
* Cette recherche est utilisée pour voir les tweets des tendances mondiales
*/
app.get('/searchTendance', (req, res) => {
  const params = { tweet_mode: 'extended', count: 50, q: req.query.query };
  client
    .get(`search/tweets`, params)
    .then(tweets => {

      res.send(tweets);
    })
    .catch(error => {
      res.send(error);
    });
});


/**
 * GET les 50 dernières tendances mondiales 
 */
app.get('/tendances', (req, res) => { 
  client
    .get(`https://api.twitter.com/1.1/trends/place.json?id=1`)
    .then(tendance => {
     
      res.send(tendance);
    })
    .catch(error => {
    res.send(error);
  }); 
});


/**
 * GET toutes les personnes que j'ai suivi (follow) 
 */
app.get('/abonnement', (req, res) => { 
  client
    .get(`friends/list`)
    .then(suggest => {
     
      res.send(suggest);
    })
    .catch(error => {
    res.send(error);
  });
    
});


/**
 * GET toutes les infos du profil 
 */
app.get('/userInfos', (req, res) => { 
  client
    .get(`https://api.twitter.com/1.1/users/show.json?screen_name=Boulch16`)
    .then(suggest => {
     
      res.send(suggest);
    })
    .catch(error => {
    res.send(error);
  });
    
});


/**
 * GET toutes les personnes qui me suivent (followers)
 */
app.get('/followers', (req, res) => { 
client
  .get(`followers/list`)
  .then(suggest => {
    
    res.send(suggest);
  })
  .catch(error => {
  res.send(error);
});
  
});


/**
 * POST : permet de poster des tweets
 */
app.post('/post_tweet', (req, res) => {
  tweet = req.body;
  client
    .post(`statuses/update`, tweet)
    .then(tweeting => {
      res.send(tweeting);
    })
    .catch(error => {
    res.send(error);
  });
});