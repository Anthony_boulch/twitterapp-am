-- INTRODUCTION

Cette application utilise l'API twitter. Elle permet d'avoir un fil d'actualité et de poster des tweets. Cette application permet également de voir les tweets dans lesquels nous sommes mentionné ainsi que les tendances mondiales. Enfin, elle permet de voir ses followers ainsi que les personnes que j'ai follow.

BACK : Node
FRONT : Angular

PS : Nécessite Node, Angular, npm


-- CLONE DU PROJET

    1. Clonez le projet du git.
        CMD : git clone https://gitlab.com/Anthony_boulch/twitterapp-am.git

-- ETAPES POUR LANCER LE BACK

    2. Ouvrez un terminal au dossier principal qui est 'twitterapp-am'.

    3. Faites la commande suivante : 
        CMD : npm install

    4. Listez les éléments de ce dossier et vérifié que vous apercevez le fichier 'docker-compose.yml'.
        CMD : ls

    5. Vous l'avez ? tapez la commande suivante pour lancer le conteneur docker : 
        CMD : docker-compose up --build -d



-- ETAPES POUR LANCER LE FRONT

    6. Allez dans le dossier angular nommé 'angularTwitter'
        CMD : cd angularTwitter/

    7. Faites la commande suivante : 
        CMD : npm install

    8. lancez la commande :
        CMD : ng serve --port 3333

    9. Lancé votre navigateur, et entrez l'url suivant :
        URL : http://localhost:3333/